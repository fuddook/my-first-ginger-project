Dim WshShell, strCurDir
Const DeleteReadOnly = false
Set WshShell = CreateObject("WScript.Shell")
strCurDir    = WshShell.CurrentDirectory
strCurDir=Replace(strCurDir,"\scripts","")
Set objFSO = CreateObject("Scripting.FileSystemObject")
Set DataFolder = objFSO.GetFolder(strCurDir & "\Power Builder\GINGER_SCREENSHOTS")

if DataFolder.Files.Count <> 0 then
objFSO.DeleteFile(strCurDir & "\Power Builder\GINGER_SCREENSHOTS\*.jpg"),DeleteReadOnly
end if