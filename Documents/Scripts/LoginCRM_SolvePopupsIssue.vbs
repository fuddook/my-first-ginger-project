'GINGER_Description Solve popups that appear duting login to CRM
'GINGER_$USERNAME
'GINGER_$PASSWORD

'Autor: Yuri Kizhner Date: 03/Mar/2017
'Use:
'Issues: 
'*.Function finishes few seconds before CRM opens (on Updating Desktop)
'*.No negative scenarios

Option Explicit
Dim counter, WshShell, USERNAME, PASSWORD

If WScript.Arguments.Count = 0 then
	WScript.Echo "Missing parameters"
Else
	USERNAME=WScript.Arguments(0)
	PASSWORD=WScript.Arguments(1)
End if

Set WshShell = WScript.CreateObject("WScript.Shell")

counter = 0
Do

	If WshShell.AppActivate("Amdocs Customer Interaction Manager") Then Exit Do
	
		If WshShell.AppActivate("Java Update Needed") Then
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys "{ENTER}"
	End If
	
	If WshShell.AppActivate("Login") Then
		WScript.Sleep 500
		WshShell.SendKeys USERNAME
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys PASSWORD
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys "{ENTER}"
	End If

	If WshShell.AppActivate("Security Warning") Then
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys " "
		WScript.Sleep 500
		WshShell.SendKeys "{TAB}"
		WScript.Sleep 500
		WshShell.SendKeys "{ENTER}"
	End If

	If WshShell.AppActivate("Security Information") Then
		WScript.Sleep 500				
		WshShell.SendKeys "{ENTER}"
	End If

	If WshShell.AppActivate("Install Java Extension") Then
		WScript.Sleep 500				
		WshShell.SendKeys "{ENTER}"
	End If

	If WshShell.AppActivate("Application Error") Then
		WScript.Sleep 500				
		WshShell.SendKeys "{ENTER}"
	End If

	If WshShell.AppActivate("Warning - Unavailable Version of Java Requested") Then
		WScript.Sleep 500
		WshShell.AppActivate("Warning - Unavailable Version of Java Requested")
		WshShell.SendKeys "{ENTER}"
	End If
	
	WScript.Sleep 2000
  counter = counter + 1

Loop Until counter = 150 Or WshShell.AppActivate("Amdocs Customer Interaction Manager")

Wscript.echo "~~~GINGER_RC_START~~~"
If WshShell.AppActivate("Amdocs Customer Interaction Manager") Then
	Wscript.echo "CRM_UP=TRUE"
Else
	Wscript.echo "CRM_UP=FALSE"
End If
Wscript.echo "~~~GINGER_RC_END~~~"
Wscript.Quit